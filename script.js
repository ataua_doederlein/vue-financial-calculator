const options = {
    method: "GET",
    headers: {
        "X-RapidAPI-Key": "296766827fmsh41779d9e96a231dp1a94e8jsn0e70b07a13ea",
        "X-RapidAPI-Host": "google-finance4.p.rapidapi.com",
    },
};
const App = Vue.createApp({
    data() {
        return {
            // inputs values
            reals: 0,
            euros: 0,
            dollars: 0,
            // conversion rates
            brlToEur: 0,
            eurToUsd: 0,
            usdToBrl: 0,
        };
    },
    methods: {
        // get data from localStorage; if not present or data older than 24h, calls fetchApiData
        getLocalstorageData() {
            const storedValues = localStorage.getItem("storedValues");
            if (storedValues) {
                const { brlToEur, eurToUsd, usdToBrl, timestamp } =
                    JSON.parse(storedValues);
                const now = Date.now();
                if (now - timestamp > 1000 * 60 * 60 * 24) {
                    this.fetchApiData();
                } else {
                    this.brlToEur = brlToEur;
                    this.eurToUsd = eurToUsd;
                    this.usdToBrl = usdToBrl;
                }
            } else {
                this.fetchApiData();
            }
        },
        fetchApiData() {
            fetch(
                "https://google-finance4.p.rapidapi.com/market-trends/?t=currencies",
                options
            )
                .then((response) => response.json())
                .then((response) => {
                    const brlEur = response.items.filter(
                        (t) => t.info.title === "BRL / EUR"
                    )[0];
                    this.brlToEur = brlEur.price.last.value;
                    const eurUsd = response.items.filter(
                        (t) => t.info.title === "EUR / USD"
                    )[0];
                    this.eurToUsd = eurUsd.price.last.value;
                    const usdBrl = response.items.filter(
                        (t) => t.info.title === "BRL / USD"
                    )[0];
                    this.usdToBrl = 1 / usdBrl.price.last.value;
                    localStorage.setItem(
                        "storedValues",
                        JSON.stringify({
                            brlToEur: this.brlToEur,
                            eurToUsd: this.eurToUsd,
                            usdToBrl: this.usdToBrl,
                            timestamp: Date.now(),
                        })
                    );
                })
                .catch((err) => {
                    alert('Something went wrong.\nCould not retrieve data from server.\nPlease, try again later.');
                    console.error(err);
                });
        },
        formatCurrency(val) {
            return val
                .replace(",", ".")
                .replace(/(\d{1,}\.{0,1}\d{0,2})(.{0,})/gm, "$1")
                .replace(/(^0)(\d{1,})/gm, "$2");
        },
        fromReals() {
            this.reals = this.formatCurrency(this.reals);
            this.euros = (this.reals * this.brlToEur).toFixed(2);
            this.dollars = (this.reals / this.usdToBrl).toFixed(2);
        },
        fromEuros() {
            this.euros = this.formatCurrency(this.euros);
            this.reals = (this.euros / this.brlToEur).toFixed(2);
            this.dollars = (this.euros * this.eurToUsd).toFixed(2);
        },
        fromDollars() {
            this.dollars = this.formatCurrency(this.dollars);
            this.reals = (this.dollars * this.usdToBrl).toFixed(2);
            this.euros = (this.dollars / this.eurToUsd).toFixed(2);
        },
    },
    // lifecycle method - get data as early as possible
    beforeMount() {
        this.getLocalstorageData();
    },
});
App.mount("#app");
